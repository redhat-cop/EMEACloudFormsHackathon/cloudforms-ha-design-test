# Database high availability for single region

This document contains the tests and observations related to a single region high availability databae setup.

## Disclaimer

This article contains content that is not at all to be considered an
official Red Hat documentation. This is totally unsupported work. It is
not meant to be working or solve anything you are facing or think or
might have as a problem in your life.

## Overview

With version 4.2, CloudForms introduced a built-in mechanism for database high availability. Prior to this, database high availability had to be implemented using classic clustering technologies - this required a dedicated database installation with the PostgreSQL version used in CloudForms, and the setup of clustering using Pacemaker and shared storage or storage replication. This brought up the need of a quorum device or fencing, introducing the full complexity of these classic high availability setups.

The built-in mechanism offers transaction based replication between a primary database and a standby database in a share-nothing architecture. The availability of the primary database is observed by a database monitor, and each connected appliance tries to switch to the standby database in case of a failure.

## Setup

For a simple test, three virtual machines have been deployed from Red Hat's CloudForms appliance templates. 

- 1 CFME database only appliance, configured as primary ("cfme1")
- 1 CFME database only appliance, configured as standby ("cfme2")
- 1 CFME UI/worker appliances, without database, configured to use primary/standdby database ("cfme3")
- all in one zone
- all in one network segment

For automatic setup of this environment via ansible see the folder `test-setup`.

Additionally, one more standby database was added. The objective was, to verify if it is possible to have multiple standby database - but that isn't possible, and will not work at all with the current implementation of replication.

Findings from the initial setup:

1. In the HA guide [1], chapter 1.1 link behind *Planning* is wrong (ends in 404). 
2. Many links point to CloudForms 4.5 instead of 4.6.
3. In the appliance_console_cli command, the configuration of application database monitor is missing - in the interactive appliance_console it's there. It should be added, to improve automation.
4. The appliance_console_cli doesn't allow to show current settings/options, no way for automatic checks, impacts automation. Customers do demand automatic provisioning of CFME appliances!
5. When configuring standby databases via appliance_console_cli (!), the PostgreSQL database doesn't get any SElinux context. Workaround: run restorecon -r /var/opt/rh (about 1000 files - avoid -v if you don't need it).
6. We are using appliance_console_cli to set a database to primary mode. The command seems to work fine, but the command ends with a message "Aborted." and a return value 134. We are calling this from Ansible, so Ansible fails. The database is working, and running as primary.
7. You cannot stop the appliance_console sometimes, and you can only interrupt it with a SIGKILL.

## Testing

### Failover

Situation:
- The primary database is running on cfme1.
- The standby database is running on cfme2. An admin user is logged in to the web UI on cfme3.

Actions:
- Now the VM running cfme1 is stopped - no shutdown, forced stop.

Results:
- After stopping the primary database, the web UI was showing an internal server error (HTTP 500) for every request.
- After several minutes, the web UI became responsive again. Content from the database is available, test passed.

The following is an example output from /var/www/miq/vmdb/log/ha_admin.log.  The database monitor recognizes that the primary database is no longer available.  After that, the configuration is changed to the standby database, and the evmserverd process is restarted:

```
E, [2018-06-21T15:43:16.581638 #27874] ERROR -- : Primary Database is not available. EVM server stop initiated. Starting to execute failover...
E, [2018-06-21T15:45:08.511657 #27874] ERROR -- : Failed to establish PG connection: could not connect to server: No route to host
        Is the server running on host "192.168.24.62" and accepting
        TCP/IP connections on port 5432?

I, [2018-06-21T15:45:08.608878 #27874]  INFO -- : Failing over to server using conninfo: {:dbname=>"vmdb_production", :user=>"root", :port=>5432, :host=>"192.168.24.141"}
I, [2018-06-21T15:45:16.800919 #27874]  INFO -- : Starting EVM server from failover monitor
```

Findings:
- The evm database monitor has a delay of 600 seconds after startup hardcoded. So any failover action will not happen in the first 600 seconds after an appliance start, or a manual restart of the monitor.
- The evm database monitor performs a restart of the evmserverd. This restart may take several minutes, in this time, the web UI is not accesible and a user will get error mesages. These are ugly error messages - first the internal server error mentioned before, later a message *service unavailable*, while the evmserverd is restarting (there is an example picture in our repo). 
- In the log/ha_admin.log, a message appears on startup of the monitor. It mentions a DB_CHECK_FREQUENCY=300. It seems the real check frequency is 60 seconds, and it is harcoded in /bin/evm_watchdog.rb.
- Replication status not visible inside CloudForms UI.

### Failback

Situation:
- The former primary dabatase on cfme1 is not active, the VM is down.
- The former standby database on cfme2 is now active, as primary.
- The web UI works normally.

Actions:
- Start the cfme1 virtual machine.
- Start the appliance_console, go to *6) Configure database replication* and choose *2) Configure server as standby*.

Results:
- After start of the former primary, no reaction of the evm monitor on our web UI was seen. The combination of primary on cfme2 (former standby) and web UI continued to work without interruption - test passed, as expected.
- The appliance_console script configured the former primary as a standby database, and enforces a full replication - test passed, works as expected.

Findings:
- For a large CloudForms database, the initial synchronization may take a lot of time, may put load on the database and the network between the database servers. This should been put into consideration when doing a failback. We haven't been able to collect better measures.
- Replication status not visible inside CloudForms UI.
- Replication direction swapped now.
- A recovery of a "broken" database can be easily be achieved by setting up a new database server from the Red Hat CloudForms template and replicating into it. There no need to re-use the old primary, but it is possible. 
- This could also be used as a scenario to migrate the database.

## Questions and Risks

1. If the network communication to the primary database fails, when will the failover happen? => TODO Martin to test this
2. What happens if the primary isn't down but comes back after a while?
3. What happens if primary and standby are both actively used concurrently by CFME appliances? How is that avoided?

## How to recognize the replication situation inside the database

The replication direction of a database can so far only be recognized in the appliance_console utility. Inside the database, the status can be recognized, too.

On the database appliance (primary or standby), connect to the CloudForms database using the psql command line client:

```
[root@cfme1 ~]# psql vmdb_production
psql (9.5.9)
Type "help" for help.

vmdb_production=#
```

This is the content of the *repl_nodes* table of the CloudForms database *vmdb_production*. This is the original situation - the replication node with the number 11 has been setup as primary, the second replication node has the number 22 and is standby:

```
vmdb_production=# select * from repl_nodes;
 id |  type   | upstream_node_id |        cluster        |      name      |                       conninfo                       |   slot_name    | priority | active 
----+---------+------------------+-----------------------+----------------+------------------------------------------------------+----------------+----------+--------
 11 | master  |                  | miq_region_10_cluster | 192.168.24.141 | host=192.168.24.141 user=root dbname=vmdb_production | repmgr_slot_11 |      100 | t
 22 | standby |               11 | miq_region_10_cluster | 192.168.24.62  | host=192.168.24.62 user=root dbname=vmdb_production  | repmgr_slot_22 |      100 | t
(2 rows)
```

The following is an example output. It has been seen on a database that was standby before, and has become primary after the former primary was shut down. There are two replication nodes, numbers 11 and 22, and both have the status *master*. This is an indicator that a failover happened and that the status of the former master hasn't been updated recently.

```
vmdb_production=# select * from repl_nodes;
 id |  type   | upstream_node_id |        cluster        |      name      |                       conninfo                       |   slot_name    | priority | active 
----+---------+------------------+-----------------------+----------------+------------------------------------------------------+----------------+----------+--------
 11 | master  |                  | miq_region_10_cluster | 192.168.24.141 | host=192.168.24.141 user=root dbname=vmdb_production | repmgr_slot_11 |      100 | t
 22 | master  |               11 | miq_region_10_cluster | 192.168.24.62  | host=192.168.24.62 user=root dbname=vmdb_production  | repmgr_slot_22 |      100 | t
(2 rows)
```

The following example output is from the database after starting the former primary and rejoining it into the setup as a standby database. The servers have "swapped" roles now, compared to the original setup.

```
vmdb_production=# select * from repl_nodes;
 id |  type   | upstream_node_id |        cluster        |      name      |                       conninfo                       |   slot_name    | priority | active 
----+---------+------------------+-----------------------+----------------+------------------------------------------------------+----------------+----------+--------
 11 | standby |                  | miq_region_10_cluster | 192.168.24.141 | host=192.168.24.141 user=root dbname=vmdb_production | repmgr_slot_11 |      100 | t
 22 | master  |               11 | miq_region_10_cluster | 192.168.24.62  | host=192.168.24.62 user=root dbname=vmdb_production  | repmgr_slot_22 |      100 | t
(2 rows)
```

## References
1. Work was based on the [CloudForms 4.6 High Availability Guide](https://access.redhat.com/documentation/en-us/red_hat_cloudforms/4.6/html-single/high_availability_guide/).
