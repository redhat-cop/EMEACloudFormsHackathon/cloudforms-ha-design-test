# CloudForms Hackathon 2018 Berlin

## Disclaimer

This article contains content that is not at all to be considered an
official Red Hat documentation. This is totally unsupported work. It is
not meant to be working or solve anything you are facing or think or
might have as a problem in your life.

## Short Summary

This track was about verifying the high availability design of CloudForms. While initially, work focused on database failover and UI/API high availability.

## Longer Story

During the first analysis, we discovered a good number of test cases:
- Database high availability for single region setup
- Database high availability for multi-region setup
- UI/API high availability
- Server role high availability

## Participants
- Dayle Parker
- Joachim von Thadden
- Martin Welk



[1] https://gitlab.com/redhat-cop/EMEACloudFormsHackathon/introduction/issues/1