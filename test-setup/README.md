# CloudForms Hackathon 2018 Berlin

## Disclaimer

This is totally unsupported work. It is not meant to be working or solve
anything you are facing or think or might have as a problem in your life.

## Test Environment

The test environment can be set up by a collection of ansible playbooks
from the lightbreeze project: https://gitlab.com/jthadden/lightbreeze

It allows you to install a 2 DB and 2 Engine infrastructure on your kvm
host.

After downloading the project add the file
`config-cfmehackathon-cfmehatest.yml` to the folder config. Add the hosts
file `hosts-cfmehackathon-cfmehatest` to the root folder as well as the
shell script `create-cfmehackathon-cfmehatest.sh`.

To install run the shell script. You will be prompted first to remove an
existing installation and then to install a repo mirror if you want to.
Last step is to install the 4 CFME server.
