#!/bin/bash

help()
{
  cat <<EOF
Usage: $1 [CUSTOMER [TAG [INFRA]]]

Parameters are used in the following way:
-i hosts\${CUSTOMER}${TAG} -e @config/config\${CUSTOMER}\${TAG}.yml -e @config/config_infrastructure\${INFRA}.yml

Defaults to:
$1 "-cfmehackathon" "-cfmehatest" ""

Example for a minimal deployment:
$1 "" "-minimal"

EOF

exit 0
}

[ "$1" = "-h" ] || [ "$1" = "--help" ] && help

CUSTOMER="$1"
[ "$CUSTOMER" = "" ] && CUSTOMER="-cfmehackathon"
export CUSTOMER

TAG="$2"
[ "$TAG" = "" ] && TAG="-cfmehatest"
export TAG

INFRA="$3"
export INFRA

echo
echo "Using: $1 \"${CUSTOMER}\" \"${TAG}\" \"${INFRA}\""
echo

echo "###########################################"
echo "# destroy all nodes"
echo "###########################################"
read -p "Do you want to delete the nodes (y/n)? " answ
[ "$answ" = "y" ] && ansible-playbook -v -i hosts${CUSTOMER}${TAG} -e @config/config${CUSTOMER}${TAG}.yml -e @config/config_infrastructure${INFRA}.yml --tags layer0,layer1 destroy.yml
echo "###########################################"
echo "# create the mirror"
echo "###########################################"
read -p "Do you want to create the mirror (y/n)? " answ
[ "$answ" = "y" ] && { ansible-playbook -v -i hosts${CUSTOMER}${TAG} -e @config/config${CUSTOMER}${TAG}.yml -e @config/config_infrastructure${INFRA}.yml --limit layer1,lxc-cfmehackathon-mirror,cfmehackathon-mirror --tags layer0,layer1,bootstrap,prep_hosts,mirror --skip-tags upload_kvmimages create.yml || exit 1; }
echo "###########################################"
echo "# create everything else"
echo "###########################################"
read -p "Do you want to create all other machines (y/n)? " answ
[ "$answ" = "y" ] && ansible-playbook -v -i hosts${CUSTOMER}${TAG} -e @config/config${CUSTOMER}${TAG}.yml -e @config/config_infrastructure${INFRA}.yml --skip-tags sync_mirror create.yml
