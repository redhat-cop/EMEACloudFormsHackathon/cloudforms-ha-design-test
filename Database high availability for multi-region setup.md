# Database high availability for multi-region setup

## Overview

This story is derived from the single-region setup, but is expected to behave different for the replication from a sub-region into the master database. As the sub-region database has no database monitor - this is part of the application server, which won't be running on a database -, the sub-region won't notice the failure of the primary master database.
Goal here is, to observe and document the implications and consequences of this behaviour.

Time ran out on this - the test cases can be easily derived from the single region setup, but will need a master database in HA configuration, and ideally also a HA sub-region to verify the various combinations of master and slave.
