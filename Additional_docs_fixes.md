# Documentation updates identified during testing

This is a list of issues that need fixing in the HA Guide, found while configuring and testing the procedures in the [High Availability guide](https://access.redhat.com/documentation/en-us/red_hat_cloudforms/4.6/html-single/high_availability_guide). This is additional information to [Database high availability for single region](https://gitlab.com/redhat-cop/EMEACloudFormsHackathon/cloudforms-ha-design-test/blob/master/Database%20high%20availability%20for%20single%20region%20setup.md).




## HA Guide changes

Found while testing the installation chapter -- [Chapter 2. Installing the Appliances](https://access.redhat.com/documentation/en-us/red_hat_cloudforms/4.6/html-single/high_availability_guide/#installation)

* In general, the step about deploying an appliance with an extra disk was a little troublesome and took some searching having not remembered how to do it offhand. Add a link to the Installing on RHV guide perhaps, and mention to 'see the installation guide for your host platform'.

### 2.1. Installing the Primary Database-Only Appliance

* Mention that this section initializes (creates) the DB. `/var/opt/rh/rh-postgresql95/lib/pgsql` is where to check on the database server (don't need to mention that). Running $psql shows you info - when the size if 7256KB it's still an empty DB.

### 2.2. Installing a CloudForms Appliance

* Mention that this step populates the DB (adding content), then you can set up replication after.
* Explain the why from the note in the text. The current first para in the note should move out of the note, and make the second sentence IMPORTANT so it's not missed:
**"This must be configured from the CloudForms appliance before the primary and secondary database-only appliances can be configured."
* Retitle this section so it is about creating a region, or is more parallel to the title "Installing Additional CloudForms Appliances" . "Installing your first CloudForms appliance".
* Step 8v -- add an Important box - All appliances in the same region must use the same v2 key.
* Step 9 has repeated text, delete from ii.: Note that creating a database region will destroy any existing data and cannot be undone.
* step ii - mention it can be number only (integer number? is there a limit to number of regions/region number?)
* add a new step after "Assign a unique database region number" - Enter port number. For example, 5432.
* Add default value example for each step here. (ie. port number, vmdb name)
* reword "For Are you sure you want to continue? Select y."
* Add "This will now populate the database" (or similar)
* Add how long this will take (a few minutes) - nice to know so you know stuff isn't going wrong. ie. is the timing enough for a coffee? lunch? "In most cases this will take a few minutes."" If it doesn't showing "running" in a few minutes, refresh the Summary Information.
* change 'name' to 'hostname'

### 2.3. Configuring the Primary Database-Only Appliance

* Make it clear you're doing this on the primary DB server you created in 2.1. Add an xref to the section.
* IN step 3 - what numbers can you use? only numbers? add an example.
* in ii) use the DB name you already set up.
* v) what is the idea behind this? (we don't know what this does but context would be nice). IP for the appliance you configured.
* Add a sentence "This configures database replication." or similar

### 2.4. Installing the Standby Database-Only Appliance
No issues.
 
### 2.5. Configuring the Standby Database-Only Appliance

* Add default example values in substeps for step 4.
* Add to step iv) : "Enter and confirm the cluster database password."
* v) the appliance you configured in which step - clarify
* "this appliance" - is it always?
* vii) Add `repmgrd` in brackets to be more clear.
* Add final text mentioning it will take a few minutes, and look at the Summary Information.


### 2.6. Installing Additional CloudForms Appliances

* Same key as previously
* vi) step 9 change formatting, and mention what this step does: to set up the second worker to join the region.
* ii) add port number step
* iii) Enter a name - add default as an example - vmdb_production
* Add confirmation step - "Activating the configuration using the following settings will take a few minutes" or similar. "This is complete when the appliance console displays a message 'Configuration activated successfully' "
* Add how to confirm? Look at the summary screen; the CFME Server will show as "running" and the CFME Database will show the names of the primary database-only appliance. (Steal the paragraph from 2.2)



## Other docs fixes found

* Discussion on appliances types - [Appliance Types](https://access.redhat.com/documentation/en-us/red_hat_cloudforms/4.6/html/deployment_planning_guide/planning#appliance-types)
"Non-CloudForms VM with database" is probably better renamed as "External database appliance" - roles are disabled and it's more consistent with our other docs. Also, the advantage of choosing the 'dedicated database' setting is that it also pre-configures HA tools for you, otherwise it's the same.




## Product bugs found

* *2.1. Installing the Primary Database-Only Appliance* - step 11. reword.
** "Choose the appliance type" in the appliance console -- works in a way that's not user friendly -- often users choose the default option, and the default (y) is to create a standalone DB server. There's potential to create this type by accident and it's unreversible (Christian Jung has seen this happen). It would be better as a question like "Choose the appliance type to create:", then answered as 3 options - 1) Regular appliance 2) Primary dedicated database 3) Standby dedicated database
* The appliance console menu option 7 is displaying incorrectly: `7) translation missing: en.advanced_settings.db_maintenance` --> Adam Grare mentioned I should lodge an issue in the appliance_console repo








