Web UI
- web server role
- have multiple appliances with web server role (in zone)
- have load balancer in front
- have SSL offloading to load balancer

Database

CloudForms Worker Roles

Assumption:
- PostgreSQL database replication is reliable

Multi-region setup
- No failover monitor on database servers
- Failover monitor only exists on worker appliances
- What is the failover monitor?
- Conclusion: a sub-region database server will not know about the master database high availability. No automatic failover. What happens with replication from sub- region to master if master fails?
- Assumption? Nothing will happen?

- What about Embedded Ansible?

Implications:
- have service items/catalogs unique names across multi-regions so that they are recognizable in the master
